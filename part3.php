

<property title="ISIN"><![CDATA[RU000A0JR225]]></property>

<property title="Эмитент"><![CDATA[Департамент по финансам, бюджету и контролю Краснодарского края]]></property>
<property title="Эмитент (кратко)"><![CDATA[краткое наименование не предусмотрено]]></property>

<property title="Дата гос. регистрации"><![CDATA[20.09.10]]></property>
<property title="Дата регистрации отчета / Уведомления об итогах"><![CDATA[]]></property>

<property title="Код гос. регистрации"><![CDATA[RU34003KND0]]></property>
<property title="Код ДКК"><![CDATA[RF0000016313]]></property>
<property title="Код РТС"><![CDATA[KNDK03]]></property>
<property title="CFI"><![CDATA[]]></property>
<property title="Лицевой счет ДКК"><![CDATA[]]></property>

<property title="Тип"><![CDATA[Облигация субъектов федерации]]></property>

<property title="Выпуск"><![CDATA[Вып. 3]]></property>

<property title="Номинал (р.)"><![CDATA[1000]]></property>

<property title="Наличие блокировок"><![CDATA[]]></property>

<property title="Вариант обслуживания"><![CDATA[Облигации (основной)]]></property>

<property title="Наименование выпускаn"><![CDATA[Обл. Краснодар.кр 03]]></property>
