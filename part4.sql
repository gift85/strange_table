USE issuers;
SELECT t.name, sum(n.name) as nominal_sum, group_concat(i.name separator ', ')
FROM issuers i
  LEFT JOIN issuer_nominal n on i.nominal_id = n.id
  LEFT JOIN issuer_types t on i.type_id = t.id
WHERE t.id = 1
GROUP BY t.name
ORDER BY nominal_sum DESC