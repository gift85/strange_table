CREATE DATABASE issuers;
USE issuers;
CREATE TABLE issuers_reg (
  id INT(11) NOT NULL AUTO_INCREMENT,
  registration_date DATE NOT NULL,
  report_date DATE NULL,
  PRIMARY KEY(id)
);
CREATE TABLE issuers_codes (
  id INT(11) NOT NULL AUTO_INCREMENT,
  registration_code VARCHAR(50) NOT NULL,
  dkk CHAR(12) NOT NULL,
  rts VARCHAR(20) NULL,
  cfi CHAR(12) NULL,
  ddk_account CHAR(12) NULL,
  PRIMARY KEY(id)
);
CREATE TABLE issuer_types (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(200) NOT NULL,
  PRIMARY KEY(id)
);
CREATE TABLE issuer_releases (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) NOT NULL,
  PRIMARY KEY(id)
);
CREATE TABLE issuer_nominal (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name DECIMAL(10, 2) NOT NULL,
  PRIMARY KEY(id)
);
CREATE TABLE issuer_blocking (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) NOT NULL,
  PRIMARY KEY(id)
);
CREATE TABLE issuer_services (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(200) NOT NULL,
  PRIMARY KEY(id)
);
CREATE TABLE issuer_release_names (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(200) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE issuers (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(200) NOT NULL,
  shortname VARCHAR(200) NOT NULL,
  isin CHAR(12) NOT NULL,
  reg_id INT(12),
  codes_id INT(12),
  type_id INT(12) NULL,
  release_id INT(12),
  nominal_id INT(12),
  block_id INT(12) NULL,
  service_id INT(12),
  release_name_id INT(12),
  PRIMARY KEY(id),
  FOREIGN KEY(reg_id) REFERENCES issuers_reg(id),
  FOREIGN KEY(codes_id) REFERENCES issuers_codes(id),
  FOREIGN KEY(type_id) REFERENCES issuer_types(id),
  FOREIGN KEY(release_id) REFERENCES issuer_releases(id),
  FOREIGN KEY(nominal_id) REFERENCES issuer_nominal(id),
  FOREIGN KEY(block_id) REFERENCES issuer_blocking(id),
  FOREIGN KEY(service_id) REFERENCES issuer_services(id),
  FOREIGN KEY(release_name_id) REFERENCES issuer_release_names(id)
);