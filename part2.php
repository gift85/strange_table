<?php

$html = file_get_contents('server_data.html');

preg_match('#<table(.*)>[\s\S]*</table>#iU', $html, $table);
preg_match('#<thead(.*)><tr>([\s\S]*)</tr></thead>#iU', $table[0], $thead);
preg_match_all('#<th(\s.*)?>([\s\S]*)</th>#iU', $thead[2], $th);
preg_match('#<tbody(.*)>([\s\S]*)</tbody>#iU', $table[0], $tbody);
preg_match_all('#<tr(\s.*)?>([\s\S]*)</tr>#iU', $tbody[2], $tbtr);
$head = array_map('convertEncoding', $th[2]);

$body = [];
foreach ($tbtr[2] as $key => $tr) {
    preg_match_all('#<td(\s.*)?>([\s\S]*)</td>#iU', $tr, $res);
    $body[$key] = array_map('trim', $res[2]);
}

$dom = new DomDocument("1.0", "utf-8");
$items = $dom->createElement('items');

foreach ($body as $row) {
    $item = $dom->createElement('item');
    foreach ($row as $key => $value) {
        $property = $dom->createElement('property');
        $property->setAttribute('title', $head[$key]);
        $property->appendChild($dom->createCDATASection($value));
        $item->appendChild($property);
    }
    $items->appendChild($item);
}

$dom->appendChild($items);
$dom->formatOutput = true;
$dom->save('emissions.xml');

function convertEncoding($item) {
    return mb_convert_encoding(trim($item), 'utf-8', "windows-1251");
}
